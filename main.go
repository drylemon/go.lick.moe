package main

import (
	"fmt"
	//"log"
	"net/http"
	"database/sql"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
)

func PrintPaste(w http.ResponseWriter, r *http.Request){
	params := mux.Vars(r)
	var paste string
	err := db.QueryRow("SELECT paste FROM pastemap WHERE id = ?", params["id"]).Scan(&paste)
	if (err == sql.ErrNoRows) {
		fmt.Fprint(w, "paste not found.")
	} else {
		fmt.Fprintf(w, paste)
	}
}

func Paste(w http.ResponseWriter, r *http.Request){
	r.ParseForm()

	text := r.Form["paste"][0]
	sum := sha256.Sum256([]byte(text))

	// string representation of truncated sha256
	shortsum := hex.EncodeToString(sum[0:4])
	fmt.Fprint(w, "lick.moe/p/"+shortsum)

	stmt, _ := db.Prepare("INSERT INTO pastemap(id, paste) values(?,?)")
	stmt.Exec(shortsum, text)
}

func ShowJSON(w http.ResponseWriter, r *http.Request){

	// initialize empty map
	pastemap := map[string]string{}
	
	var (
		id []byte
		paste string
	) 

	rows, _ := db.Query("SELECT * FROM pastemap")

	for rows.Next() {
	
		// get values from query result
		rows.Scan(&id, &paste)
		
		// cast paste id to string
		strid := string(id[:])
		// fmt.Fprint(w, strid)
		
		// map paste id to paste
		pastemap[strid] = paste
	}

	// print json
	json.NewEncoder(w).Encode(pastemap)

}

var db *sql.DB

func main() {
	// database shit
	db, _ = sql.Open("sqlite3", "pastedb")
	stmt, _ := db.Prepare("CREATE TABLE IF NOT EXISTS pastemap (id VARCHAR(8), paste TEXT)")
	stmt.Exec()

	router := mux.NewRouter()
	router.HandleFunc("/p/{id}", PrintPaste).Methods("GET")
	router.HandleFunc("/paste", Paste).Methods("POST")
	router.HandleFunc("/json", ShowJSON).Methods("GET")
	router.PathPrefix("/").Handler(http.FileServer(http.Dir("./static/")))
	http.Handle("/", router)
	http.ListenAndServe(":8000", router)
}
